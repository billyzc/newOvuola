var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload'),
    fileinclude = require('gulp-file-include');

gulp.task('images', function () {
    return gulp.src('./images/**/*')
        .pipe(cache(imagemin({
            optimizationLevel: 3,
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest('./dist/images'))
        .pipe(notify({
            message: 'Images task complete'
        }));
});

gulp.task('script', function () {
    return gulp.src('./js/**/*.js')
        .on('error', function (err) {
            console.log(err.message);
        })
        .pipe(gulp.dest('./dist/js'))
});
gulp.task('styles', function () {
    return sass('./style/**/*.scss')
        .on('error', function (err) {
            console.log(err.message);
        })
        .pipe(gulp.dest('./dist/css'))
        .pipe(livereload());
});

gulp.task('fileinclude', function () {
    gulp.src(['./pages/**/*.html']) //主文件
        .pipe(fileinclude({
            prefix: '@@', //变量前缀 @@include
            basepath: './pages/_include', //引用文件路径
            indent: true //保留文件的缩进
        }))
        .pipe(gulp.dest('./dist')); //输出文件路径
});

gulp.task('clean', d => {
    if (clean('dist')) {
        gulp.src('dist').pipe(clean());
    }
    d();
});

gulp.task('watch', function () {
    gulp.watch(['./style/**/*.scss'], ['styles']);
    gulp.watch(['./pages/**/*.html'], ['fileinclude']);
    gulp.watch(['./js/**/*.js'], ['script']);
    gulp.watch(['./images/**/*'], ['images']);
})